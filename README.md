# Angular & Typescript Schulung by [theCodeCampus](https://www.thecodecampus.de/)

# TypeScript
* Grundlagen
* Klassen & Interfaces
* Module
# Tooling
* Modul-Systeme
* TypeScript-Compiler & Linting
* Build-System

# Angular
* Komponenten – Grundlagen
* Module
* Bootstrapping
* Komponenten & Data-Binding
* Direktiven
* Komponenten verschachteln
* Template-Syntax
* Services
* Dependency Injection
* Komponenten-Lebenszyklus
* Pipes
* Asynchrone & Reaktive Programmierung
* Routing
* Formulare
* Validierung
* Komponenten-Interaktion
* Datenfluss
* Kommunikation mit dem Backend
# Testen
* Spec-Tests

Schulung powered by [theCodeCampus](https://www.thecodecampus.de/)