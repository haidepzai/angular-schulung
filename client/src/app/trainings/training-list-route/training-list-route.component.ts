import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TrainingSelectedEvent } from '../training-list/training-list.component';
import { Training } from '../training.entity';
import { TrainingService } from '../training.service';

@Component({
  selector: 'app-training-list-route',
  templateUrl: './training-list-route.component.html',
  styleUrls: ['./training-list-route.component.css']
})
export class TrainingListRouteComponent implements OnInit {
  public trainings$: Observable<Training[]>;

  selectedTraining: Training;

  constructor(private trainingService: TrainingService) {
  }

  ngOnInit(): void {
    this.trainings$ = this.trainingService.getAll();
  }  

  saveTraining(event: TrainingSelectedEvent): void {
    this.selectedTraining = event.training;
    console.log(event.training);
  }

}
