import { Training } from './../training.entity';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { TrainingService } from '../training.service';

@Component({
  selector: 'app-training-details-route',
  templateUrl: './training-details-route.component.html',
  styleUrls: ['./training-details-route.component.css']
})
export class TrainingDetailsRouteComponent implements OnInit {

  training$: Observable<Training>;

  constructor(private route: ActivatedRoute, private trainingService: TrainingService) { }

  ngOnInit(): void {
    console.log("hi")
    this.training$ = this.route.params.pipe(
      map(params => parseInt(params['id'], 10)),
      switchMap(id => this.trainingService.getById(id))
    )
  }

  onUpdateTraining(event: Training): void {
    console.log(event);
    this.trainingService.update(event.id, event);
  }

}
