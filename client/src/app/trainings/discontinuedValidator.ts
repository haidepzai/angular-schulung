import { FormGroup, ValidationErrors } from '@angular/forms';
import { parseISO, isFuture, startOfDay } from 'date-fns';

export function DiscontinuedValidator(
  discontinuedControlName: string,
  nextRunControlName: string
): ValidationErrors | null {
  return (group: FormGroup) => {
    const discontinuedControl = group.controls[discontinuedControlName];
    const nextRunControl = group.controls[nextRunControlName];

    const discontinuedValue = discontinuedControl.value;
    const nextRunValue = parseISO(nextRunControl.value);
    const valid = !discontinuedValue || !isFuture(startOfDay(nextRunValue));

    return valid ? null : { discontinuedWithFutureNextRun: true };
  };
}
