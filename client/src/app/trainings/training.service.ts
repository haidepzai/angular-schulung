import { Training } from './training.entity';
import { Injectable } from '@angular/core';
import { TRAININGS } from './training.mock';
import { Observable, ReplaySubject } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { compareAsc, parseISO, isFuture, startOfDay } from 'date-fns';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TrainingService {
  private trainingsSubject = new ReplaySubject<Training[]>(1);
  private cached: boolean = false;

  constructor(private http: HttpClient) {
    this.trainingsSubject.next(TRAININGS);
  }

  load(): void {
    this.http.get<Training[]>('http://localhost:3000/api/training')
      .subscribe(ts => this.trainingsSubject.next(ts));
  }

  getAll(): Observable<Training[]> {
    if (!this.cached) {
      this.load();
      this.cached = true;
    }
    return this.trainingsSubject;
  }

  getById(id: number): Observable<Training> {
    return this.getAll().pipe(
      map((trainings) => {
        const training = trainings.find((t) => t.id === id);
        if (training) {
          return training;
        } else {
          throw new Error(`Could not find training with id ${id}`);
        }
      })
    );
  }

  update(id: number, data: Partial<Training>): void {
    this.http.patch(`http://localhost:3000/api/training/${id}`, data)
      .subscribe(() => this.reload());
  }

  reload() {
    this.http.get<Training[]>('http://localhost:3000/api/training')
      .subscribe(ts => this.trainingsSubject.next(ts));
  }

  getNext(): Observable<Training> {
    return this.getAll().pipe(
      map((trainings) => trainings.filter(nextRunInFuturePredicate)),
      map((trainings) => trainings.sort(nextRunComparator)),
      map((trainings) => (trainings.length > 0 ? trainings[0] : undefined))
    );
  }
}

function nextRunComparator(t1: Training, t2: Training): number {
  return compareAsc(parseISO(t1.nextRun), parseISO(t2.nextRun));
}
function nextRunInFuturePredicate(t: Training): boolean {
  return isFuture(startOfDay(parseISO(t.nextRun)));
}
