import { Training } from './training.entity';

export const TRAININGS: Training[] = [
  {
    id: 1,
    name: 'Angular Basic',
    description: 'Basic Schulung',
    imageUrl: './../assets/images/trainings/angular2-shield.svg',
    discontinued: false,
    nextRun: "2021-08-31"
  },
  {
    id: 2,
    name: 'Angular Advanced',
    description: 'Advanced Schulung',
    discontinued: true,
    nextRun: "2020-07-18"
  },
  {
    id: 3,
    name: 'TypeScript',
    imageUrl: './../../assets/images/trainings/typescript-logo.svg',
    discontinued: false,
    nextRun: "2021-08-10"
  }
];
