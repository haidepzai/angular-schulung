import { TrainingService } from './../training.service';
import { map, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Training } from './../training.entity';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DiscontinuedValidator } from '../discontinuedValidator';

@Component({
  selector: 'app-training-details',
  templateUrl: './training-details.component.html',
  styleUrls: ['./training-details.component.css']
})
export class TrainingDetailsComponent implements OnInit {
  public form: FormGroup;
  @Input() training: Training;

  @Output() updatedTraining = new EventEmitter<Training>();

  constructor() { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(this.training.name, Validators.required),
      nextRun: new FormControl(this.training.nextRun),
      description: new FormControl(this.training.description, Validators.minLength(10)),
      discontinued: new FormControl(this.training.discontinued)
    }, DiscontinuedValidator("discontinued", "nextRun"));
  }

  checkValue(): void {
    console.log(this.form)
  }

  onSubmit(): void {
      this.updatedTraining.emit({ ...this.training, ...this.form.value }); //2 Objekte mergen, wobei neue Werte ersetzt werden
      /**       
        let origObjectOne = {a: 1, b: 2, c: 3};
        let origObjectTwo = {a: 4, e: 5, f: 6}; 

        let mergedObject = {...origObjectOne, ...origObjectTwo};
        {a: 4, b: 2, c: 3, e: 5, f: 6}
       */
  }

}
