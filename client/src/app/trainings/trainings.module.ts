import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingListComponent } from './training-list/training-list.component';
import { TrainingDetailsComponent } from './training-details/training-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../shared/material-module';
import { TakePlaceSoonPipe } from './take-place-soon.pipe';
import { TrainingListRouteComponent } from './training-list-route/training-list-route.component';
import { TrainingDetailsRouteComponent } from './training-details-route/training-details-route.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    //Alle Components aufgelistet für dieses Modul
    TrainingListComponent,
    TrainingDetailsComponent,
    TakePlaceSoonPipe,
    TrainingListRouteComponent,
    TrainingDetailsRouteComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports: [
    //Components exportieren, damit sie in anderen Modulen eingebunden werden können
    TrainingListComponent,
    TrainingDetailsComponent,
  ],
})
export class TrainingsModule {}
