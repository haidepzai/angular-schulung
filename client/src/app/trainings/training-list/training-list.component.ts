import { Training } from './../training.entity';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-training-list',
  templateUrl: './training-list.component.html',
  styleUrls: ['./training-list.component.css']
})
export class TrainingListComponent implements OnInit {
  @Input() trainings: Training[];
  @Output() trainingSelected = new EventEmitter<TrainingSelectedEvent>();

  constructor() { }

  ngOnInit(): void {
  }

  onListItemClicked(event: MouseEvent, training: Training): void {
    const selectedEvent = new TrainingSelectedEvent(training);
    this.trainingSelected.emit(selectedEvent);
  }

}

export class TrainingSelectedEvent {
  constructor (public readonly training: Training) {}
}
