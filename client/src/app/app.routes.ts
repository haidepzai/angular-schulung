import { TrainingDetailsRouteComponent } from './trainings/training-details-route/training-details-route.component';
import { TrainingListComponent } from './trainings/training-list/training-list.component';
import { Routes } from '@angular/router';

export const routes: Routes = [
    { path: 'training', component: TrainingListComponent },
    { path: 'training/:id', component: TrainingDetailsRouteComponent},
    { path: '**', redirectTo: '/training'}
]