import { TrainingService } from './trainings/training.service';
import { Training } from './trainings/training.entity';
import { Component, OnInit } from '@angular/core';
import { TrainingSelectedEvent } from './trainings/training-list/training-list.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  nextTrainings$: Observable<Training>;

  constructor(private trainingService: TrainingService) {
  }

  ngOnInit(): void {
    this.nextTrainings$ = this.trainingService.getNext();
  }

}
