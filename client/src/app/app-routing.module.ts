import { TrainingListRouteComponent } from './trainings/training-list-route/training-list-route.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrainingDetailsRouteComponent } from './trainings/training-details-route/training-details-route.component';

export const routes: Routes = [
  { path: 'training', component: TrainingListRouteComponent },
    { path: 'training/:id', component: TrainingDetailsRouteComponent},
    { path: '**', redirectTo: '/training'}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
